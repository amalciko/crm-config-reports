import { CONSTANTS } from "crm-utilities";
const {
  CONFIG_REPORT: { ACTION_TYPE },
} = CONSTANTS;

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("ConfigReports", {
      id: {
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      configId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      requestId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      actionType: {
        type: Sequelize.ENUM,
        values: [ACTION_TYPE.TAKE, ACTION_TYPE.UNTAKE, ACTION_TYPE.PUSHED],
      },
      fullName: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      regionId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      cityId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      updatedAt: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
      },
    }, { schema: "public" });
  },

  async down (queryInterface) {
    await queryInterface.dropTable("ConfigReports");
  },
};
