import { GenericObject } from "moleculer";

export namespace iConfigReports{

    export interface MetaData {
        userId: number;
        companyId: number;
        companyRole: string;
        departmentId: number;
        departmentRole: string;
        permissions: object[];
        permissionsGroups: object[];
        allGroupsPermissions: object[];
        departments: number[];
    }

    export interface BaseCtx {
        meta: { data: MetaData };
        call<T, P extends GenericObject = GenericObject>(actionName: string, params?: P, opts?: GenericObject): PromiseLike<T>;
    }

    export type ActionType = "take" | "untake" | "pushed";
    export type ChartsQueryParamsGroupBy = "fullName" | "actionType";

    export interface GetChartsQueryParams {
        end: Date;
        start: Date;
        groupBy: ChartsQueryParamsGroupBy;
        regionId?: number;
        cityId?: number;
        configId?: number;
        actionType?: ActionType;
    }

    export interface GetConfigChartsWhere {
        createdAt?: object;
        regionId?: number;
        cityId?: number;
        actionType?: string;
        configId?: number;
    }

    export interface RetChartProps {
        name: string;
        count: string;
    }

    export interface ConfigReportCtx extends BaseCtx {
        params: ConfigReportParams;
    }
    export interface ConfigReportParams {
        configId: number;
        requestId: number;
        actionType: ActionType;
        fullName: string;
        regionId: number;
        cityId: number;
    }

    export interface GetChartByConfigCtx extends BaseCtx {
        params: GetChartByConfigParams;
    }
    export interface GetChartByConfigParams {
        start: Date;
        end: Date;
        regionId?: number;
        cityId?: number;
    }

    export interface GetChartByActionCtx extends BaseCtx {
        params: GetChartByActionParams;
    }
    export interface GetChartByActionParams {
        configId: number;
        regionId?: number;
        cityId?: number;
        start: Date;
        end: Date;

    }
}

