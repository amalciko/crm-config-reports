import { iConfigReports } from "../types";

import db from "../db/models";
const { Sequelize: { Op, literal } } = db;


class QueryBuilder {

  static getChartsQuery(queryParams: iConfigReports.GetChartsQueryParams) {
    const { actionType, cityId, configId, end, groupBy, regionId, start } = queryParams;

    const where: iConfigReports.GetConfigChartsWhere = {
      createdAt: { [Op.and]: [{ [Op.gte]: start }, { [Op.lte]: end }] },
    };

    if (configId) {
      where.configId = configId;
    }
    if (actionType) {
      where.actionType = actionType;
    }
    if (regionId) {
      where.regionId = regionId;
      if (cityId) {
        where.cityId = cityId;
      }
    }

    return {
      where,
      group: [groupBy],
      order: [["count", "DESC"]],
      attributes: [[groupBy, "name"], [literal("COUNT(id)"), "count"]],
    };
  }
}

export default QueryBuilder;
