const { ERRORS: { ERR_CODE, ERR_MSG } } = require("../config");

export class Exception extends Error {
    public message;
    public code;
    constructor(error) {
      super();
      this.message = ERR_MSG.TRY_AGAIN;
      this.code = ERR_CODE.CONFLICT;
      if (error instanceof GetPException) {
        this.message = error.message;
        this.code = error.code;
      }
    }
}

export class GetPException extends Error {
    public message;
    public code;
    constructor(message, code) {
      super();
      this.message = message;
      this.code = code;
    }
}
