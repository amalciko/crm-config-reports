import ConfigReportsController from "../controllers/configReportsController";
import { iConfigReports } from "../types";


class ConfigReportsManager {
  public constructor() {
    this.createConfigReport = this.createConfigReport.bind(this);
    this.getConfChartByConfigs = this.getConfChartByConfigs.bind(this);
    this.getSingleConfChartByAction = this.getSingleConfChartByAction.bind(this);
  }

  public async createConfigReport(ctx: iConfigReports.ConfigReportCtx): Promise<{ message: string }> {
    const { params } = ctx;
    await ConfigReportsController.createConfigReport(params);
    return { message: "Config report successfully created" };
  }

  public async getConfChartByConfigs(ctx: iConfigReports.GetChartByConfigCtx): Promise<iConfigReports.RetChartProps[]> {
    const { params } = ctx;
    return await ConfigReportsController.countTakeRequest(params);
  }

  public async getSingleConfChartByAction(ctx: iConfigReports.GetChartByActionCtx): Promise<iConfigReports.RetChartProps[]> {
    const { params } = ctx;
    return await ConfigReportsController.getActionsCount(params);
  }
}
export default new ConfigReportsManager();
