"use strict";

import { Service } from "moleculer";
import configReportsManager from "../managers/configReportsManager";

import { CONSTANTS } from "crm-utilities";
const { CONFIG_REPORT: { ACTION_TYPE } } = CONSTANTS;

class ConfigReportsService extends Service {
  constructor(broker) {
    super(broker);
    this.configReportsManager = configReportsManager;
    this.parseServiceSchema({
      name: "configReportsService",
      version: "v1",
      actions: {
        createConfigReport: {
          requiredCompanyAuth: true,
          params: {
            configId: { type: "number", optional: false, integer: true, positive: true, convert: true },
            requestId: { type: "number", optional: false, integer: true, positive: true, convert: true },
            actionType: { type: "enum", values: [ACTION_TYPE.PUSHED, ACTION_TYPE.TAKE, ACTION_TYPE.UNTAKE], optional: false, empty: false },
            fullName: { type: "string", optional: false, empty: false },
            regionId: { type: "number", optional: false, integer: true, positive: true, convert: true },
            cityId: { type: "number", optional: false, integer: true, positive: true, convert: true },
          },
          handler: this.configReportsManager.createConfigReport,
        },
        
        getConfChartByConfigs: {
          requiredCompanyAuth: true,
          params: {
            start: { type: "date", optional: false, convert: true },
            end: { type: "date", optional: false, convert: true },
            regionId: { type: "number", optional: true, integer: true, positive: true, convert: true },
            cityId: { type: "number", optional: true, integer: true, positive: true, convert: true },
          },
          handler: this.configReportsManager.getConfChartByConfigs,
        },

        getSingleConfChartByAction: {
          requiredCompanyAuth: true,
          params: {
            configId: { type: "number", optional: false, empty: false, integer: true, positive: true, convert: true },
            start: { type: "date", optional: false, convert: true },
            end: { type: "date", optional: false, convert: true },
            regionId: { type: "number", optional: true, integer: true, positive: true, convert: true },
            cityId: { type: "number", optional: true, integer: true, positive: true, convert: true },
          },
          handler: this.configReportsManager.getSingleConfChartByAction,
        },
      },
    });
  }
}

export = ConfigReportsService;
