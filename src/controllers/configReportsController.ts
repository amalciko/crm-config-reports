import QueryBuilder from "../helpers/queryBuilder";
import { Exception } from "../helpers/exceptions";
import { iConfigReports } from "../types";

import { CONSTANTS } from "crm-utilities";
const { CONFIG_REPORT: { ACTION_TYPE, CHART_GROUP_BY } } = CONSTANTS;

import db from "../db/models";
const configReportsModel = db.ConfigReports;


class ConfigReportsController {
  public constructor() {
    this.createConfigReport = this.createConfigReport.bind(this);
    this.countTakeRequest = this.countTakeRequest.bind(this);
    this.getActionsCount = this.getActionsCount.bind(this);
  }

  public async createConfigReport(reportData: iConfigReports.ConfigReportParams): Promise<void> {
    try {
      await configReportsModel.create(reportData);
    } catch (err) {
      throw new Exception(err);
    }
  }

  public async countTakeRequest(params: iConfigReports.GetChartByConfigParams): Promise<iConfigReports.RetChartProps[]> {
    try {
      const queryParams = {
        end: params.end,
        start: params.start,
        cityId: params.cityId,
        regionId: params.regionId,
        actionType: ACTION_TYPE.TAKE,
        groupBy: CHART_GROUP_BY.FULLNAME,
      };
      const query = QueryBuilder.getChartsQuery(queryParams);
      return await configReportsModel.findAll(query);

    } catch (err) {
      throw new Exception(err);
    }
  }

  public async getActionsCount(params: iConfigReports.GetChartByActionParams): Promise<iConfigReports.RetChartProps[]> {
    try {
      const queryParams = {
        end: params.end,
        start: params.start,
        cityId: params.cityId,
        regionId: params.regionId,
        configId: params.configId,
        groupBy: CHART_GROUP_BY.ACTION_TYPE,
      };
      const query = QueryBuilder.getChartsQuery(queryParams);
      return await configReportsModel.findAll(query);

    } catch (err) {
      throw new Exception(err);
    }
  }
}

export default new ConfigReportsController();
